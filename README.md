Electron with ES6 and SCSS
=================

the boilerplate for Electron with ES6 and SCSS


## Description
- Gulpについて
  - GulpタスクをES6(import文とか)で書くのにbabel-preset-es2015を記述した.babelrcが必要
  - ファイル名をgulpfile.babel.jsにすると自動でbabel-registerがインストールされ、ES6で書ける
  - 手動でやるならrequire('babel-register')のあとES6のタスクファイルを読み込む
  - Gulpタスクでasync-await使うのにbabel-preset-stage-3が必要
  - あとbabel-polyfillも必要 (regeneratorRuntime is not definedってなる)

- Rollupについて
  - 詳しくは [browserify から rollup.js に乗り換えてる](http://hadashia.hatenablog.com/entry/2016/08/08/131046) 参照
  - rollupは1つのファイルをentryとし、そこにimportされるファイルを全て1つのファイルに展開してまとめる
  - rollup-plugin-node-resolveはnode_modulesからimportしたファイルもバンドルしてくれる
  - rollup-plugin-commonjsはNPMもジュールのCommonJS記法をES6に書き換えてくれる
  - すべてES6 Moduleに書き換えてからのバンドル



## Installations
- Node、NPM

  どこかで調べてインストールしてください。


- Gulp

  v4.0使ってます。  
  \#付けるとmaster以外のブランチからインストールできるみたいです。

```
$ npm i -g "https://github.com/gulpjs/gulp.git#4.0"
```


- Ruby, Gem, Bundler
  
  SCSS-lintの利用に必要です。  
  RubyとGemはどこかで調べてインストールしてください。  
  BundlerはGemでインストールできます。

```
$ gem install bundler
```


- Wine

  MacかLinuxでWindows用のビルドを行おうとすると必要になります。  
  適当にインストールしておいてください。 ※めっちゃ時間かかる



## Usage
- 初期化

```
$ npm run setup
```


- 開発

```
$ npm run serve
```

Developer ToolsでJSのファイルが表示されないときはリロードしてください。


- リリースビルド

```
$ npm run production
```


- リリース (パッケージング)

```
$ npm run release:win32
$ npm run release:darwin
$ npm run release:linux

# for win32, darwin, linux
$ npm run release
```



## Directory structure

```
.
├── app                     Compiled files
│   ├── dev                 For development
│   │   ├── browser
│   │   └── renderer
│   │       ├── css
│   │       └── js
│   └── prod                For production
│       ├── browser
│       └── renderer
│           ├── css
│           └── js
├── release                 Packaging applications
│   ├── darwin
│   ├── linux
│   └── win32
└── src                     Source directory
    ├── browser             For browser process files
    └── renderer            For renderer process files
        ├── js
        └── scss
```



## Author
[hennin](https://gitlab.com/hennin)