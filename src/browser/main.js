import { app, BrowserWindow } from 'electron'
import log from './log'

let mainWindow

process.on('error', (err) => {
    // console.log(err)
    log.info(err)
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('ready', () => {
    mainWindow = new BrowserWindow({ width: 800, height: 600 })
    mainWindow.loadURL('file://' + __dirname + '/../renderer/index.html')

    mainWindow.on('closed', () => {
        mainWindow = null
    })
})
