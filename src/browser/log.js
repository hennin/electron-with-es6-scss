import log from 'electron-log'
import fs  from 'fs'

log.transports.file.level = 'warning'
log.transports.file.format = '{h}:{i}:{s}:{ms} {text}'
log.transports.file.maxSize = 5 * 1024 * 1024
log.transports.file.file = __dirname + '/log.txt'
log.transports.file.streamConfig = { flags: 'w' }
log.transports.file.stream = fs.createWriteStream('log.txt')

export default log
