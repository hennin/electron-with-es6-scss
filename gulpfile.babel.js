import 'babel-polyfill'
import del               from 'del'
import { server }        from 'electron-connect'
import packager          from 'electron-packager'
import fs                from 'fs'
import gulp              from 'gulp'
import gulpBabel         from 'gulp-babel'
import gulpCssmin        from 'gulp-cssmin'
import gulpEslint        from 'gulp-eslint'
import gulpHtmlmin       from 'gulp-htmlmin'
import gulpPlumber       from 'gulp-plumber'
import gulpPostcss       from 'gulp-postcss'
import gulpRollup        from 'gulp-rollup'
import gulpSass          from 'gulp-sass'
import gulpScssLint      from 'gulp-scss-lint'
import gulpSourcemaps    from 'gulp-sourcemaps'
import gulpUglify        from 'gulp-uglify'
import gulpUseref        from 'gulp-useref'
import gulpWatch         from 'gulp-watch'  // gulp.watch invocate electron.reload just once
import postcssCssnext    from 'postcss-cssnext'
import rollupCommonjs    from 'rollup-plugin-commonjs'
import rollupNodeResolve from 'rollup-plugin-node-resolve'


const gulpClean = (...args) => () => del(...args)

const srcs = {
    'html':              'src/renderer/index.html',
    'scss':              'src/renderer/scss/**/*.scss',
    'scss_entry':        'src/renderer/scss/style.scss',
    'js:renderer':       'src/renderer/js/**/*.js',
    'js:renderer_entry': 'src/renderer/js/app.js',
    'js:browser':        'src/browser/**/*.js'
}
const dev  = 'app/dev'
const prod = 'app/prod'
const dests = {
    'dev:html':              `${dev}/renderer`,
    'prod:html':             `${prod}/renderer`,
    'dev:css':               `${dev}/renderer/css`,
    'prod:css':              `${prod}/renderer/css`,
    'dev:js:renderer':       `${dev}/renderer/js`,
    'prod:js:renderer':      `${prod}/renderer/js`,
    'dev:js:browser':        `${dev}/browser`,
    'prod:js:browser':       `${prod}/browser`
}
const buildFiles = {
    'dev:renderer':  [ `${dests['dev:html']}/index.html`,
                       `${dests['dev:css']}/style.css`,
                       `${dests['dev:js:renderer']}/app.js` ],
    'prod:renderer': [ `${dests['prod:html']}/index.html`,
                       `${dests['prod:css']}/style.css`,
                       `${dests['prod:js:renderer']}/app.js` ],
    'dev:browser':   `${dests['dev:js:browser']}/**/*.js`,
    'prod:browser':  `${dests['prod:js:browser']}/**/*.js`
}


/* clean */
gulp.task('clean:dev',  gulpClean(dev))
gulp.task('clean:prod', gulpClean(prod))
gulp.task('clean',      gulpClean('app'))


/* html */
gulp.task('dev:html', () => gulp
    .src(srcs['html'])
    .pipe(gulpPlumber())
    .pipe(gulp.dest(dests['dev:html'])))

gulp.task('prod:html', () => gulp
    .src(srcs['html'])
    .pipe(gulpPlumber())
    .pipe(gulpUseref())  // remove from <!-- build:remove --> to <!-- endbuild -->
    .pipe(gulpHtmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(dests['prod:html'])))


/* scss */
gulp.task('dev:scss', () => gulp
    .src(srcs['scss_entry'])
    .pipe(gulpSourcemaps.init())
        .pipe(gulpSass())
        .pipe(gulpPostcss([ postcssCssnext() ]))
    .pipe(gulpSourcemaps.write('./'))  // relative to dest
    .pipe(gulp.dest(dests['dev:css'])))

gulp.task('prod:scss', () => gulp
    .src(srcs['scss_entry'])
    .pipe(gulpSass())
    .pipe(gulpPostcss([ postcssCssnext() ]))
    .pipe(gulpCssmin())
    .pipe(gulp.dest(dests['prod:css'])))


/* renderer process */
const rollupOptions = {
    allowRealFiles: true,  // to avoid the hypothetical file system error
    entry: srcs['js:renderer_entry'],
    plugins: [ rollupNodeResolve(),
               rollupCommonjs() ]
}
gulp.task('dev:js:renderer', () => gulp
    .src(srcs['js:renderer'])
    .pipe(gulpPlumber())
    .pipe(gulpSourcemaps.init())
        .pipe(gulpRollup(rollupOptions))
        .pipe(gulpBabel())
    .pipe(gulpSourcemaps.write('./'))
    .pipe(gulp.dest(dests['dev:js:renderer'])))

gulp.task('prod:js:renderer', () => gulp
    .src(srcs['js:renderer'])
    .pipe(gulpPlumber())
    .pipe(gulpRollup(rollupOptions))
    .pipe(gulpBabel())
    .pipe(gulpUglify())
    .pipe(gulp.dest(dests['prod:js:renderer'])))


/* browser process */
gulp.task('dev:js:browser', () => gulp
    .src(srcs['js:browser'])
    .pipe(gulpPlumber())
    .pipe(gulpBabel())
    .pipe(gulp.dest(dests['dev:js:browser'])))

gulp.task('prod:js:browser', () => gulp
    .src(srcs['js:browser'])
    .pipe(gulpPlumber())
    .pipe(gulpBabel())
    .pipe(gulp.dest(dests['prod:js:browser'])))


/* package.json */
import config from './package.json'
const createPackageJson = (properties, dir) => done => {
    let config = require('./package.json'),
        result = {}
    result.main = 'browser/main.js'
    properties.forEach((p) => { result[p] = config[p] })
    fs.writeFile(`${dir}/package.json`,
                 JSON.stringify(result, null, '    '),
                 err => { if (err) console.log(err) })
    done()
}
gulp.task('dev:config', createPackageJson([ 'name', 'version' ], dev))
gulp.task('prod:config',
          createPackageJson([ 'name', 'version', 'description',
                              'repository', 'author', 'license',
                              'bugs', 'homepage' ],
                            prod))


/* build */
gulp.task('build:dev', gulp.series(
    gulp.parallel('dev:html', 'dev:scss',
                  'dev:js:renderer', 'dev:js:browser'),
    'dev:config'
))
gulp.task('build:prod', gulp.series(
    gulp.parallel('prod:html', 'prod:scss',
                  'prod:js:renderer', 'prod:js:browser'),
    'prod:config'
))
gulp.task('cleanBuild:dev',  gulp.series('clean:dev',  'build:dev'))
gulp.task('cleanBuild:prod', gulp.series('clean:prod', 'build:prod'))


/* lint */

// SCSS Lint
gulp.task('lint:scss', () => gulp
    .src(srcs['scss'])
    .pipe(gulpScssLint({ 'reporterOutputFormat': 'Checkstyle',
			                   'bundleExec': true,
			                   'config': 'scss-lint.yml' }))
    .pipe(gulpScssLint.failReporter()))

// ES Lint
const eslint = src => () => gulp
    .src(src)
    .pipe(gulpPlumber())
    .pipe(gulpEslint({ useEslintrc: true }))
    .pipe(gulpEslint.format())
    .pipe(gulpEslint.failOnError())

gulp.task('lint:js:browser',  eslint(srcs['js:browser']))
gulp.task('lint:js:renderer', eslint(srcs['js:renderer']))
gulp.task('lint:js', gulp.parallel('lint:js:browser',
                                   'lint:js:renderer'))

gulp.task('lint', gulp.parallel('lint:scss', 'lint:js'))


/* serve */
gulp.task('serve', () => {
    const electron = server.create()
    electron.start('app/dev')

    gulpWatch(srcs['html'],        gulp.task('dev:html'))
    gulpWatch(srcs['scss'],        gulp.parallel('lint:scss',
                                                 'dev:scss'))
    gulpWatch(srcs['js:renderer'], gulp.parallel('lint:js:renderer',
                                                 'dev:js:renderer'))
    gulpWatch(srcs['js:browser'],  gulp.parallel('lint:js:browser',
                                                 'dev:js:browser'))

    gulpWatch(buildFiles['dev:renderer'], electron.reload)
    gulpWatch(buildFiles['dev:browser'],  electron.restart)
})


/* release */
const packaging = platform => done => {
    packager({
        dir: 'app/prod',
        out: `release/${platform}`,
        platform: platform,
        arch: 'x64',
        overwrite: true
    }, err => { if (err) console.log(err) })
    done()
}

gulp.task('release:win32',  packaging('win32'))
gulp.task('release:darwin', packaging('darwin'))
gulp.task('release:linux',  packaging('linux'))
gulp.task('release', gulp.parallel('release:win32',
                                   'release:darwin',
                                   'release:linux'))


/* default */
gulp.task('default', gulp.series('build:dev', 'serve'))
